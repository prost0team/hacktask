package prost0team.com.hacktask

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by root on 20.04.18.
 */
object DateUtils {
    val ONE_DAY_IN_MILLS = 1000L * 60 * 60 * 24
    val ONE_MONTH_IN_MILLS = ONE_DAY_IN_MILLS * 31

    fun generateTaskDateString(startTime: Long, endTime: Long): String {
        val formatter = SimpleDateFormat("d MMM HH:mm", Locale.getDefault())
        val sb = StringBuilder(SimpleDateFormat("EEE", Locale.getDefault()).format(Date(startTime)).toUpperCase())
        sb.append(": ")
        sb.append("${formatter.format(Date(startTime))} - ${formatter.format(Date(endTime))}")
        return sb.toString()
    }
}