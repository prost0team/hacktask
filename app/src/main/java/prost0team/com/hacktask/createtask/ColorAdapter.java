package prost0team.com.hacktask.createtask;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import prost0team.com.hacktask.R;
import prost0team.com.hacktask.model.TagViewModel;

public class ColorAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<TagViewModel> items;
    private final int mResource;

    public ColorAdapter(@NonNull Context context, @LayoutRes int resource,
                              @NonNull List items) {
        super(context, resource, 0, items);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        this.items = items;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TagViewModel tag = items.get(position);
        ImageView color = view.findViewById(R.id.color);
        color.setBackgroundColor(Color.parseColor(tag.getColor()));
        TextView title = view.findViewById(R.id.title);
        title.setText(tag.getName());
        return view;
    }
}