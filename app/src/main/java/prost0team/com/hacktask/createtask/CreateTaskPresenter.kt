package prost0team.com.hacktask.createtask

import android.util.Log
import com.google.android.gms.tasks.OnFailureListener
import prost0team.com.hacktask.base.BasePresenter
import prost0team.com.hacktask.db.DbService
import prost0team.com.hacktask.db.DbTask
import prost0team.com.hacktask.model.RequestViewModel
import prost0team.com.hacktask.model.TagViewModel
import prost0team.com.hacktask.model.TaskViewModel
import java.util.*

class CreateTaskPresenter(private val view: ICreateTaskView) : BasePresenter(view) {
    public var startDate: Calendar = Calendar.getInstance()
    public var endDate: Calendar = Calendar.getInstance()
    public var deadlineDate: Calendar = Calendar.getInstance()

    var model: RequestViewModel? = null
    override fun onCreate() {
        super.onCreate()
//        DbService.getAllTags()
//                .map {
//                    val list = ArrayList<TagViewModel>()
//                    it.forEach { dbTag ->
//                        list.add(TagViewModel.toViewModelForm(dbTag))
//                    }
//                    list
//                }.subscribe { tags ->
//                    view.showTags(tags)
//                }.connect()
    }

    override fun authChangeListener() {
        super.authChangeListener()
        view.disableSecondTab(firebaseUser == null)
    }

    fun createTask(text: String, type: Byte, isSounded: Boolean, status: Byte, period: Int, tag: TagViewModel, hasDeadline: Boolean) {
        var dbTask: TaskViewModel
        if (type == 0.toByte() || !hasDeadline)
            dbTask = TaskViewModel(UUID.randomUUID().toString(), text, startDate.timeInMillis, endDate.timeInMillis, null, isSounded, type, status, period, tag)
        else
            dbTask = TaskViewModel(UUID.randomUUID().toString(), text, startDate.timeInMillis, endDate.timeInMillis, deadlineDate.timeInMillis, isSounded, type, status, period, tag)
        DbService.createTask(TaskViewModel.toDbForm(dbTask))
                .subscribe { task ->
                    sendToFcm(task)
                    view.closeActivity();
                }.connect()
    }

    fun getRecommendationsByEmail(email: String) {

    }

    private fun sendToFcm(dbTask: DbTask) {
        if (dbTask.sounded) {
            view.alarm(dbTask.uuid, dbTask.text, dbTask.startTime)
        }
        if (firebaseUser != null) {
            firestore.collection("tasks")
                    .document(dbTask.uuid)
                    .set(TaskViewModel.toFcmForm(dbTask, firebaseUser!!))
                    .addOnCompleteListener {
                        Log.e("ok", "complete")
                    }
                    .addOnFailureListener(OnFailureListener {
                        Log.e("ok", "fail", it)

                    })
        }
    }

    fun createRequest(text: String, type: Byte, hasDeadLine: Boolean, emails: MutableList<String>) {
        DbService.saveHistory(emails,Calendar.getInstance().timeInMillis)
        Log.e("ok", "createRequest")
        if (firebaseUser != null) {

            emails.forEach { email ->
                Log.e("email = ", email)
                firestore.collection("requests")
                        .add(RequestViewModel.toFcmForm(text, type, deadlineDate.timeInMillis, startDate.timeInMillis, endDate.timeInMillis, firebaseUser!!.email!!, email, hasDeadLine))
                        .addOnCompleteListener {
                            Log.e("ok", "complete")
                            view.closeActivity()
                        }
                        .addOnFailureListener(OnFailureListener {
                            Log.e("ok", "fail", it)

                        })
            }
        }
    }

    override fun createRequestListener() {

    }

    fun saveEditted(text: String, hasDeadline: Boolean, isSounded: Boolean,tag:TagViewModel) {
        var dbTask: TaskViewModel
        if (model!!.type == 0.toByte() || !hasDeadline)
            dbTask = TaskViewModel(UUID.randomUUID().toString(), text, startDate.timeInMillis, endDate.timeInMillis, null, isSounded, model!!.type, 0, 0, tag)
        else
            dbTask = TaskViewModel(UUID.randomUUID().toString(), text, startDate.timeInMillis, endDate.timeInMillis, deadlineDate.timeInMillis,  isSounded, model!!.type, 0, 0, tag)
        DbService.createTask(TaskViewModel.toDbForm(dbTask))
                .subscribe { task ->
                    sendToFcm(task)
                    view.closeActivity();
                }.connect()
    }
}

