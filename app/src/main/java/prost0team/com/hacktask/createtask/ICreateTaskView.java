package prost0team.com.hacktask.createtask;

import prost0team.com.hacktask.base.BaseView;

public interface ICreateTaskView extends BaseView {
    void showTags();

    void closeActivity();

    void disableSecondTab(boolean b);

    void alarm(String uuid, String text, Long time);
}
