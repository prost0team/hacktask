package prost0team.com.hacktask.createtask

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import kotlinx.android.synthetic.main.activity_task_create.*
import prost0team.com.hacktask.R
import prost0team.com.hacktask.base.BaseActivity
import prost0team.com.hacktask.db.DbService
import prost0team.com.hacktask.model.RequestViewModel
import prost0team.com.hacktask.model.TagViewModel
import prost0team.com.hacktask.utils.TodoNotificationService
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreateTaskActivity : BaseActivity(), ICreateTaskView {
    override fun closeActivity() {
        finish()
    }

    override fun showTags() {
        val tags = ArrayList<TagViewModel>()
        tags.add(TagViewModel("Макс.", "#BF360C"))
        tags.add(TagViewModel("Высокий", "#FF9800"))
        tags.add(TagViewModel("Обычный", "#FFD54F"))
        tags.add(TagViewModel("Низкий", "#81C784"))
        tags.add(TagViewModel("Мин.", "#388E3C"))
        val tagAdapter = ColorAdapter(this, R.layout.tag_item, tags)
        tagAdapter.setDropDownViewResource(R.layout.tag_item)
        group.adapter = tagAdapter
    }

    var emails: MutableList<EditText> = ArrayList()

    override val presenter: CreateTaskPresenter = CreateTaskPresenter(this)
    override var contentViewLayoutRes: Int = R.layout.activity_task_create
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.model = intent.getSerializableExtra("model") as RequestViewModel?
        if (presenter.model != null) {
            task_type.visibility = View.GONE
            task_owner.visibility = View.GONE
            email_layout.visibility = View.GONE
            if (presenter.model!!.type == 0.toByte()) {
                toolbar.title = "Событие от " + presenter.model!!.emailFrom
                deadline_layout.visibility = View.GONE
            } else {
                toolbar.title = "Задача от " + presenter.model!!.emailFrom
            }
            task_title.setText(presenter.model!!.text)
            if (presenter.model!!.startTime != null) {
                var calendar = Calendar.getInstance()
                calendar.timeInMillis = presenter.model!!.startTime!!
                presenter.startDate = calendar

                val dateFormat: String
                if (DateFormat.is24HourFormat(this@CreateTaskActivity)) {
                    dateFormat = "k:mm"
                } else {
                    dateFormat = "h:mm a"

                }
                start_time.setText(formatDate(dateFormat, presenter.startDate.time))
                start_date.setText(formatDate("d MMM, yyyy", presenter.startDate.time))
            }
            if (presenter.model!!.endTime != null) {
                var calendar = Calendar.getInstance()
                calendar.timeInMillis = presenter.model!!.endTime!!
                presenter.endDate = calendar

                val dateFormat: String
                if (DateFormat.is24HourFormat(this@CreateTaskActivity)) {
                    dateFormat = "k:mm"
                } else {
                    dateFormat = "h:mm a"

                }
                end_time.setText(formatDate(dateFormat, presenter.endDate.time))
                end_date.setText(formatDate("d MMM, yyyy", presenter.endDate.time))
            }
            if (presenter.model!!.deadline != null) {
                var calendar = Calendar.getInstance()
                calendar.timeInMillis = presenter.model!!.deadline!!
                presenter.deadlineDate = calendar

                val dateFormat: String
                if (DateFormat.is24HourFormat(this@CreateTaskActivity)) {
                    dateFormat = "k:mm"
                } else {
                    dateFormat = "h:mm a"

                }
                deadline_time.setText(formatDate(dateFormat, presenter.deadlineDate.time))
                deadline_date.setText(formatDate("d MMM, yyyy", presenter.deadlineDate.time))
            }
            period_layout.visibility = View.GONE
        }
        toolbar.setNavigationOnClickListener({
            finish()
        })
        task_owner.addTab(task_owner.newTab().setIcon(R.drawable.ic_person).setText("Для себя"))
        task_owner.addTab(task_owner.newTab().setIcon(R.drawable.ic_people).setText("Для других"))
        start_date.setOnClickListener({

            hideKeyboard(start_date)
            val year = presenter.startDate.get(Calendar.YEAR)
            val month = presenter.startDate.get(Calendar.MONTH)
            val day = presenter.startDate.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog.newInstance({ v, y, m, d ->
                presenter.startDate.set(Calendar.YEAR, y)
                presenter.startDate.set(Calendar.DAY_OF_MONTH, d)
                presenter.startDate.set(Calendar.MONTH, m)
                start_date.setText("с " + formatDate("d MMM, yyyy", presenter.startDate!!.time))
            }, year, month, day)
            datePickerDialog.show(fragmentManager, "DateFragment")
        })
        end_date.setOnClickListener({

            val date: Date = presenter.endDate.time
            hideKeyboard(start_date)
            val calendar = Calendar.getInstance()
            calendar.time = date
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog.newInstance({ v, y, m, d ->
                presenter.endDate.set(Calendar.YEAR, y)
                presenter.endDate.set(Calendar.DAY_OF_MONTH, d)
                presenter.endDate.set(Calendar.MONTH, m)
                end_date.setText("до " + formatDate("d MMM, yyyy", presenter.endDate!!.time))
            }, year, month, day)
            datePickerDialog.show(getFragmentManager(), "DateFragment")
        })
        deadline_date.setOnClickListener({

            hideKeyboard(deadline_date)
            val year = presenter.deadlineDate.get(Calendar.YEAR)
            val month = presenter.deadlineDate.get(Calendar.MONTH)
            val day = presenter.deadlineDate.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog.newInstance({ v, y, m, d ->
                presenter.deadlineDate.set(Calendar.YEAR, y)
                presenter.deadlineDate.set(Calendar.DAY_OF_MONTH, d)
                presenter.deadlineDate.set(Calendar.MONTH, m)
                deadline_date.setText(formatDate("d MMM, yyyy", presenter.deadlineDate.time))
            }, year, month, day)
            datePickerDialog.show(fragmentManager, "DateFragment")
        })
        email_hint.setOnClickListener {
            var hintString = email_hint.text.toString()
            var hints: MutableList<String> = hintString.split("\n").toMutableList()
            var emailsTxt: MutableList<String> = ArrayList()
            var emptyEditTexts: MutableList<EditText> = ArrayList()
            emails.forEach {
                if (!it.text.toString().isEmpty())
                    emailsTxt.add(it.text.toString())
                else
                    emptyEditTexts.add(it)
            }
//            var i = 0
//            emptyEditTexts.forEach {
//                it.setText(hints.get(i))
//                i++
//            }

            hints.forEach {
                emails.get(emails.size-1).setText(it)
                //                if (!emailsTxt.contains(it)) {
//                    addNewEditText(it)
//                }
            }
        }
        start_time.setOnClickListener(View.OnClickListener {
            val date: Date = presenter.startDate.time
            hideKeyboard(start_time)
            val calendar = Calendar.getInstance()
            calendar.time = date
            val hour = calendar.get(Calendar.HOUR_OF_DAY)
            val minute = calendar.get(Calendar.MINUTE)

            val timePickerDialog = TimePickerDialog.newInstance({ v, h, m ->
                presenter.startDate.set(Calendar.HOUR_OF_DAY, h)
                presenter.startDate.set(Calendar.MINUTE, m)

                val dateFormat: String
                if (DateFormat.is24HourFormat(this@CreateTaskActivity)) {
                    dateFormat = "k:mm"
                } else {
                    dateFormat = "h:mm a"

                }
                start_time.setText(formatDate(dateFormat, presenter.startDate.time))
            }, hour, minute, DateFormat.is24HourFormat(this@CreateTaskActivity))
            timePickerDialog.show(getFragmentManager(), "TimeFragment")
        })
        end_time.setOnClickListener(View.OnClickListener {
            val date: Date = presenter.endDate.time
            hideKeyboard(end_time)
            val calendar = Calendar.getInstance()
            calendar.time = date
            val hour = calendar.get(Calendar.HOUR_OF_DAY)
            val minute = calendar.get(Calendar.MINUTE)

            val timePickerDialog = TimePickerDialog.newInstance({ v, h, m ->
                presenter.endDate.set(Calendar.HOUR_OF_DAY, h)
                presenter.endDate.set(Calendar.MINUTE, m)

                val dateFormat: String
                if (DateFormat.is24HourFormat(this@CreateTaskActivity)) {
                    dateFormat = "k:mm"
                } else {
                    dateFormat = "h:mm a"

                }
                end_time.setText(formatDate(dateFormat, presenter.endDate.time))
            }, hour, minute, DateFormat.is24HourFormat(this@CreateTaskActivity))
            timePickerDialog.show(getFragmentManager(), "TimeFragment")
        })
        deadline_time.setOnClickListener(View.OnClickListener {
            hideKeyboard(deadline_time)
            val hour = presenter.deadlineDate.get(Calendar.HOUR_OF_DAY)
            val minute = presenter.deadlineDate.get(Calendar.MINUTE)

            val timePickerDialog = TimePickerDialog.newInstance({ v, h, m ->
                presenter.deadlineDate.set(Calendar.HOUR_OF_DAY, h)
                presenter.deadlineDate.set(Calendar.MINUTE, m)

                val dateFormat: String
                if (DateFormat.is24HourFormat(this@CreateTaskActivity)) {
                    dateFormat = "k:mm"
                } else {
                    dateFormat = "h:mm a"

                }
                deadline_time.setText(formatDate(dateFormat, presenter.deadlineDate.time))
            }, hour, minute, DateFormat.is24HourFormat(this@CreateTaskActivity))
            timePickerDialog.show(fragmentManager, "TimeFragment")
        })
        val typeData = arrayOf("Задача", "Событие")

        showTags()

        val typeAdapter = ArrayAdapter<String>(this, R.layout.spinner_item, typeData)
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        task_type.adapter = typeAdapter
        task_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                recreateVisibility()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

                // sometimes you need nothing here
            }
        }
        val periodData = arrayOf("Один раз", "Каждый день", "Один раз в два дня", "Каждую неделю", "Один раз в месяц")

        val periodAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, periodData)
        periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        period.adapter = periodAdapter
        save_btn.setOnClickListener({
            if (presenter.model == null) {
                if (task_owner.selectedTabPosition == 0) {
                    createTask()
                } else {
                    createRequest()
                }
            } else {
                saveEditted()
            }
        })
        task_owner.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                recreateVisibility()
            }
        })
        email1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                addNewEditText()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        emails.add(email1)
    }

    private fun saveEditted() {
        presenter.saveEditted(task_title.text.toString(), !deadline_date.text.toString().isEmpty(), alarm.isChecked, group.adapter.getItem(group.selectedItemPosition) as TagViewModel)
    }

    public fun addNewEditText() {
        showHintEmails()
        var k: Int = 0
        emails.forEach {
            if (!it.text.toString().isEmpty())
                k += 1
        }
        if (k == emails.size) {

            var view = layoutInflater.inflate(R.layout.edit_email, email_list, false)
            var editText: EditText = view.findViewWithTag("edit")
            emails.add(editText)
            var close: View = view.findViewWithTag("close")
            close.setOnClickListener({
                emails.remove(editText)
                email_list.removeView(view)
            })
            editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    addNewEditText()
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
            email_list.addView(view)
        }
    }

    public fun addNewEditText(text: String) {
        showHintEmails()
        var view = layoutInflater.inflate(R.layout.edit_email, email_list, false)
        var editText: EditText = view.findViewWithTag("edit")
        emails.add(editText)
        var close: View = view.findViewWithTag("close")
        close.setOnClickListener({
            emails.remove(editText)
            email_list.removeView(view)
        })
        editText.setText(text)
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                addNewEditText()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        email_list.addView(view)
    }

    override fun alarm(uuid: String, text: String, time: Long) {
        val i = Intent(this, TodoNotificationService::class.java)
        i.putExtra(TodoNotificationService.TODOUUID, UUID.fromString(uuid))
        i.putExtra(TodoNotificationService.TODOTEXT, text)
        createAlarm(i, uuid.hashCode(), time)
    }

    private fun createAlarm(i: Intent, requestCode: Int, timeInMillis: Long) {
        Log.d("#MY ", "alarm setted ${timeInMillis - System.currentTimeMillis()}")
        val am = getAlarmManager()
        val pi = PendingIntent.getService(this, requestCode, i, PendingIntent.FLAG_UPDATE_CURRENT)
        am.set(AlarmManager.RTC_WAKEUP, timeInMillis, pi)
    }

    private fun getAlarmManager(): AlarmManager {
        return getSystemService(ALARM_SERVICE) as AlarmManager
    }

    private fun showHintEmails() {
        var emailsTxt: MutableList<String> = ArrayList()
        emails.forEach {
            if (!it.text.toString().isEmpty())
                emailsTxt.add(it.text.toString())
        }
        Log.e("log","size = " + emailsTxt.size)
        var emailHintList: List<String> = DbService.getMostPriorityEmailsByEmail(emailsTxt)
        Log.e("log","hint = " + emailHintList.size)
        if (emailHintList.isEmpty()) {
            email_hint.visibility = View.GONE
        } else {
            email_hint.visibility = View.VISIBLE
            var hintString: String = ""
            emailHintList.forEach {
                hintString += it + "\n"
            }
            email_hint.text = hintString
        }
    }

    private fun recreateVisibility() {
        if (task_owner.selectedTabPosition == 0) {
            email_layout.visibility = View.GONE
            start_layout.visibility = View.VISIBLE
            alarm_layout.visibility = View.VISIBLE
            end_layout.visibility = View.VISIBLE
            group_layout.visibility = View.VISIBLE
            period_layout.visibility = View.VISIBLE
            if (task_type.selectedItemPosition == 0) {
                deadline_layout.visibility = View.VISIBLE
            } else {
                deadline_layout.visibility = View.GONE
            }
        } else {
            email_layout.visibility = View.VISIBLE
            alarm_layout.visibility = View.GONE
            group_layout.visibility = View.GONE
            period_layout.visibility = View.GONE
            if (task_type.selectedItemPosition == 0) {
                start_layout.visibility = View.GONE
                end_layout.visibility = View.GONE
                deadline_layout.visibility = View.VISIBLE
            } else {
                start_layout.visibility = View.VISIBLE
                end_layout.visibility = View.VISIBLE
                deadline_layout.visibility = View.GONE
            }
        }
    }

    private fun createRequest() {
        Log.e("hello", "Hello")
        var emailText: MutableList<String> = ArrayList()
        var k = 0
        emails.forEach { email ->
            if (!email.text.toString().isEmpty()) {
                emailText.add(email.text.toString())
            } else {
                k += 1
            }
        }
        if (k == emails.size) {
            email1.error = "Должен быть добавлен хотя бы один email"
            return
        }

        var type: Byte = 0
        when (task_type.selectedItemPosition) {
            0 -> type = 1.toByte()
            1 -> type = 0.toByte()
        }
        if (type == 0.toByte()) {
            if (start_date.text.isEmpty()) {
                start_date.error = "Поле обязательно к заполнению"
                return
            }
            if (end_date.text.isEmpty()) {
                end_date.error = "Поле обязательно к заполнению"
                return
            }
            if (start_time.text.isEmpty()) {
                start_time.error = "Поле обязательно к заполнению"
                return
            }
            if (end_time.text.isEmpty()) {
                end_time.error = "Поле обязательно к заполнению"
                return
            }
        }

        presenter.createRequest(task_title.text.toString(), type, !deadline_date.text.isEmpty(), emailText)

    }

    fun createTask() {
        if (start_date.text.isEmpty()) {
            start_date.error = "Поле обязательно к заполнению"
            return
        }
        if (end_date.text.isEmpty()) {
            end_date.error = "Поле обязательно к заполнению"
            return
        }
        if (start_time.text.isEmpty()) {
            start_time.error = "Поле обязательно к заполнению"
            return
        }
        if (end_time.text.isEmpty()) {
            end_time.error = "Поле обязательно к заполнению"
            return
        }
        if (task_title.text.isEmpty()) {
            task_title.error = "Поле обязательно к заполнению"
            return
        }
        var type: Byte = 0
        when (task_type.selectedItemPosition) {
            0 -> type = 1.toByte()
            1 -> type = 0.toByte()
        }
        var days = 0
        when (period.selectedItemPosition) {
            0 -> days = 0
            1 -> days = 1
            2 -> days = 2
            3 -> days = 7
            4 -> days = 30
        }
        presenter.createTask(task_title.text.toString(), type, alarm.isChecked, 0, days, group.adapter.getItem(group.selectedItemPosition) as TagViewModel, !deadline_date.text.isEmpty())
    }

    fun formatDate(formatString: String, dateToFormat: Date): String {
        val simpleDateFormat = SimpleDateFormat(formatString)
        return simpleDateFormat.format(dateToFormat)
    }


    fun hideKeyboard(et: EditText) {

        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(et.windowToken, 0)
    }


    override fun disableSecondTab(disable: Boolean) {
        val tabStrip = task_owner.getChildAt(0) as LinearLayout
        for (i in 0 until tabStrip.childCount) {
            tabStrip.getChildAt(i).setOnTouchListener { v, event ->
                if (disable) {
                    if (i == 1)
                        Toast.makeText(this@CreateTaskActivity, "Для того чтобы отправить другим задание, нужно зарегистрироваться", Toast.LENGTH_LONG).show()
                    true
                } else {
                    false
                }
            }
        }
    }
}
