package prost0team.com.hacktask.pin

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import prost0team.com.hacktask.MyApp
import java.nio.charset.Charset
import java.security.SecureRandom

/**
 * Created by root on 20.04.18.
 */
object PinHelper {
    private val symbols = "ABCDEFGJKLMNPRSTUVWXYZ0123456789"

    fun getPrefs(): SharedPreferences {
        return MyApp.instance.getSharedPreferences("default", Context.MODE_PRIVATE)
    }

    fun getSault(): String? {
        val string = getPrefs().getString(KEY_SAULT, "")
        return if (string.isBlank())
            null
        else
            string
    }

    fun generateSault(): String {
        val sault = ByteArray(64)
        SecureRandom().nextBytes(sault)
//        val saultBuilder = StringBuilder()
//        for (i in 0 until 64) {
//            saultBuilder.append(symbols[random.nextInt(symbols.length)])
//        }
//        val sault = saultBuilder.toString()
        val strSault = String(sault, Charset.defaultCharset())
        Log.d("#MY ", "sault was generated $strSault")
        getPrefs()
                .edit()
                .putString(KEY_SAULT, strSault)
                .apply()
        return strSault
    }

    fun pinEntered() {
        getPrefs()
                .edit()
                .putBoolean(KEY_WITH_PIN, true)
                .apply()
    }

    fun pinNotEntered() {
        getPrefs()
                .edit()
                .putBoolean(KEY_WITH_PIN, false)
                .apply()
    }

    fun isWithPin(): Boolean {
        return getPrefs().getBoolean(KEY_WITH_PIN, false)
    }

    private val KEY_SAULT = "KEY_SAULT"
    private val KEY_WITH_PIN = "KEY_WITH_PIN"

    var pin: String = ""
}