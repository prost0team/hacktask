package prost0team.com.hacktask.pin

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import com.andrognito.pinlockview.PinLockListener
import kotlinx.android.synthetic.main.activity_pin.*
import prost0team.com.hacktask.R
import prost0team.com.hacktask.base.BaseActivity
import prost0team.com.hacktask.db.DbService
import prost0team.com.hacktask.tasklist.TaskListActivity
import java.lang.Exception

/**
 * Created by root on 20.04.18.
 */
class PinActivity : BaseActivity(), IPinView {
    override val presenter: PinPresenter = PinPresenter(this)
    var enteredPin: String = ""
    var isFirstLaunch = PinHelper.getSault().isNullOrBlank()
    var stage: Stages = if (isFirstLaunch) Stages.FIRST_ENTERING else Stages.ENTERING
    override var contentViewLayoutRes: Int = R.layout.activity_pin

    enum class Stages {
        FIRST_ENTERING,
        FIRST_CONFIRMATION,
        ENTERING
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "on create")
        PinHelper.pin = ""
        isFirstLaunch = PinHelper.getSault().isNullOrBlank()
        stage = if (isFirstLaunch) Stages.FIRST_ENTERING else Stages.ENTERING
        DbService.config = null

        super.onCreate(savedInstanceState)
        refreshPinView()
        Log.e("create", "Create")

        pin_not_enter_btn.setOnClickListener {
            PinHelper.generateSault()
            PinHelper.pinNotEntered()
            startTaskList()
        }

        pin_lock_view.setPinLockListener(object : PinLockListener {
            override fun onEmpty() {
                Log.d(TAG, "empty")
            }

            override fun onComplete(pin: String) {
                when (stage) {
                    Stages.FIRST_ENTERING -> {
                        stage = Stages.FIRST_CONFIRMATION
                        enteredPin = pin
                        refreshPinView()

                    }
                    Stages.FIRST_CONFIRMATION -> {
                        if (pin != enteredPin) {
                            Toast.makeText(this@PinActivity, "Пин-коды не совпадают", Toast.LENGTH_LONG).show()
                            stage = Stages.FIRST_ENTERING
                            refreshPinView()
                        } else {
                            PinHelper.generateSault()
                            PinHelper.pin = pin
                            PinHelper.pinEntered()
                            startTaskList()
                        }
                    }
                    Stages.ENTERING -> {
                        PinHelper.pin = pin
                        try {
                            DbService.getEncryptedInstance()
                            startTaskList()
                        } catch (e: Exception) {
                            DbService.config = null
                            stage = Stages.ENTERING
                            refreshPinView()
                            Toast.makeText(this@PinActivity, "Неверный пин-код", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

            override fun onPinChange(pinLength: Int, intermediatePin: String?) {
            }
        })
        pin_lock_view.attachIndicatorDots(pin_indicator_dots)
    }

    private fun refreshPinView() {
        pin_lock_view.resetPinLockView()
        when (stage) {
            Stages.FIRST_ENTERING -> {
                animatedChangeText("Задайте пин-код для защиты данных")
                pin_not_enter_btn.visibility = View.VISIBLE
                pin_container.visibility = View.VISIBLE
            }
            Stages.FIRST_CONFIRMATION -> {
                animatedChangeText("Подтвердите пин-код")
                pin_not_enter_btn.visibility = View.VISIBLE
                pin_container.visibility = View.VISIBLE
            }
            Stages.ENTERING -> {
                pin_not_enter_btn.visibility = View.GONE
                if (PinHelper.isWithPin()) {
                    animatedChangeText("Введите пин-код")
                    pin_container.visibility = View.VISIBLE
                } else {
                    startTaskList()
                }
            }
        }
    }

    private fun startTaskList() {
        Handler().postDelayed({
            val intent = Intent(this@PinActivity, TaskListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }, 200)
    }

    private fun animatedChangeText(newText: String) {
        pin_text_status.animate().alpha(0.0f).setDuration(200).withEndAction {
            pin_text_status.text = newText
            pin_text_status.animate().alpha(1.0f).setDuration(200).start()
        }.start()
    }

    companion object {
        val TAG = "#MY " + PinActivity::class.java.simpleName
    }
}