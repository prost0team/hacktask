package prost0team.com.hacktask

import android.app.Application
import android.content.Context
import android.util.Log
import io.realm.Realm

/**
 * Created by root on 20.04.18.
 */
class MyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "app created")
        instance = this
        Realm.init(this)
    }

    companion object {
        val TAG = "#MY " + MyApp::class.java.simpleName
        lateinit var instance: Context
    }
}