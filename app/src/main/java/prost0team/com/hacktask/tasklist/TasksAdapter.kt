package prost0team.com.hacktask.tasklist

import android.app.AlertDialog
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_task.view.*
import prost0team.com.hacktask.DateUtils
import prost0team.com.hacktask.R
import prost0team.com.hacktask.db.DbService
import prost0team.com.hacktask.model.TaskViewModel
import java.util.*

/**
 * Created by root on 20.04.18.
 */
class TasksAdapter: RecyclerView.Adapter<TasksAdapter.TaskViewHolder>() {
    private val tasks = ArrayList<TaskViewModel>(32)

    fun swap(newList: List<TaskViewModel>) {
        tasks.clear()
        tasks.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(tasks[position])
    }

    inner class TaskViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(task: TaskViewModel) {
            itemView.item_task_text.text = task.text
            itemView.item_task_date.text = DateUtils.generateTaskDateString(task.startTime, task.endTime)
//            itemView.item_task_tag_name.setBackgroundColor(Color.parseColor(task.tag.color))
            itemView.item_task.setCardBackgroundColor(Color.parseColor(task.tag.color))
            itemView.item_task_tag_name.text = (task.tag.name + " приоритет").toUpperCase()
//            val c = Calendar.getInstance()
//            c.timeInMillis = task.startTime
//            val hours = c.get(Calendar.HOUR_OF_DAY)
//            Log.d(TAG, "hours = $hours")
//            when (hours) {
//                in 5..11 -> itemView.setBackgroundResource(R.drawable.morning2)
//                in 11..19 -> itemView.setBackgroundResource(R.drawable.day)
//                else -> itemView.setBackgroundResource(R.drawable.evening2)
//            }
            if (task.isSounded) {
                itemView.item_task_sound_img.visibility = View.VISIBLE
            } else {
                itemView.item_task_sound_img.visibility = View.INVISIBLE
            }
            itemView.item_task_remove_img.setOnClickListener {
                val dialog = AlertDialog.Builder(itemView.context)
                        .setCancelable(false)
                        .setTitle("Вы уверены?")
                        .setPositiveButton("Удалить", {_, _ ->
                            DbService.deleteTask(task.uuid)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({
                                        val positions = ArrayList<Int>(tasks.size)
                                        (0 until tasks.size).filterTo(positions) { tasks[it].uuid == task.uuid }
                                        if (positions.isNotEmpty()) {
                                            for (i in positions.size - 1 downTo 0) {
                                                tasks.removeAt(positions[i])
                                                notifyItemRemoved(positions[i])
                                            }
                                        }
                                    }, { error -> Log.e(TAG, error.message, error)})
                        })
                        .setNegativeButton("Отмена", {dialog, _ ->
                            dialog.dismiss()
                        })
                dialog.show()
            }
        }
    }

    companion object {
        val TAG = "#MY " + TasksAdapter::class.java.simpleName
    }
}