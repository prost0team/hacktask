package prost0team.com.hacktask.tasklist

import prost0team.com.hacktask.base.BaseView
import prost0team.com.hacktask.model.TaskViewModel

/**
 * Created by root on 20.04.18.
 */
interface ITasksListView : BaseView {
    fun showTasks(tasks: List<TaskViewModel>)
}