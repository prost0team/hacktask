package prost0team.com.hacktask.tasklist

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_task_list.*
import prost0team.com.hacktask.DateUtils
import prost0team.com.hacktask.R
import prost0team.com.hacktask.base.BaseActivity
import prost0team.com.hacktask.createtask.CreateTaskActivity
import prost0team.com.hacktask.db.DbService
import prost0team.com.hacktask.db.DbTag
import prost0team.com.hacktask.db.DbTask
import prost0team.com.hacktask.model.TaskViewModel
import prost0team.com.hacktask.settings.SettingsActivity


/**
 * Created by root on 20.04.18.
 */
class TaskListActivity : BaseActivity(), ITasksListView {
    override var contentViewLayoutRes: Int = R.layout.activity_task_list
    override val presenter: TaskListPresenter = TaskListPresenter(this)
    private var mAdapter: TasksAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, DbService.getEncryptedInstance().path)

//        DbService.dropDb()
//        Thread.sleep(200)
        mAdapter = TasksAdapter()
        tasks_list_rv.adapter = mAdapter
        tasks_list_rv.layoutManager = LinearLayoutManager(this)
//        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        add_btn.setOnClickListener {
            startActivity(Intent(this@TaskListActivity, CreateTaskActivity::class.java))
        }

//        test1()
//        test()
    }

    override fun onStart() {
        super.onStart()
        presenter.getTasks()
    }

    override fun showTasks(tasks: List<TaskViewModel>) {
        if (tasks.isEmpty()) {
            tasks_no_data.visibility = View.VISIBLE
        } else {
            tasks_no_data.visibility = View.GONE
        }
        mAdapter?.swap(tasks)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tasks, menu)
        menu.getItem(0).setOnMenuItemClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        }
        return true
    }

    fun test() {
        DbService.createTask(DbTask(
                "Помыть посуду",
                System.currentTimeMillis(),
                System.currentTimeMillis() + 10000,
                null,
                false,
                TaskViewModel.Companion.DbTypes.EVENT.type,
                TaskViewModel.Companion.DbStatuses.NOT_COMPLETED.status,
                14,
                DbTag("Дом", "#66bb6a")
        ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it ->
                    Log.d(TAG, "created task ${TaskViewModel.toViewModelForm(it)}")
                    DbService.createTask(DbTask(
                            "Отправить отчет",
                            System.currentTimeMillis() - DateUtils.ONE_MONTH_IN_MILLS * 2,
                            System.currentTimeMillis() - DateUtils.ONE_MONTH_IN_MILLS * 2 + 10000,
                            null,
                            true,
                            TaskViewModel.Companion.DbTypes.TASK.type,
                            TaskViewModel.Companion.DbStatuses.NOT_COMPLETED.status,
                            15,
                            DbTag("Работа", "#7e57c2")
                    ))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ it ->
                                presenter.getTasks()
//                                DbService.getAllTasks()
//                                        .subscribeOn(Schedulers.io())
//                                        .observeOn(AndroidSchedulers.mainThread())
//                                        .subscribe({tasks ->
//                                            tasks.map { TaskViewModel.toViewModelForm(it) }.forEach {
//                                                Log.d(TAG, "$it")
//                                            }
//
//                                            DbService.deleteTask(tasks.first().uuid)
//                                                    .subscribeOn(Schedulers.io())
//                                                    .observeOn(AndroidSchedulers.mainThread())
//                                                    .subscribe({ it ->
//                                                        DbService.getAllTasks()
//                                                                .subscribeOn(Schedulers.io())
//                                                                .observeOn(AndroidSchedulers.mainThread())
//                                                                .subscribe({ tasks ->
//                                                                    tasks.map { TaskViewModel.toViewModelForm(it) }.forEach {
//                                                                        Log.d(TAG, "@$it")
//                                                                    }
//                                                                })
//                                                    })
//                                        })
                            })

                })
    }

    fun test1() {
        DbService.saveHistory(listOf("1", "2", "4", "3"))
        DbService.saveHistory(listOf("1", "2", "5"), time = System.currentTimeMillis() - prost0team.com.hacktask.DateUtils.ONE_MONTH_IN_MILLS)
        DbService.saveHistory(listOf("1", "2", "5"), time = System.currentTimeMillis() - prost0team.com.hacktask.DateUtils.ONE_MONTH_IN_MILLS * 2 - 3600000)
        DbService.saveHistory(listOf("7", "6", "5"), time = System.currentTimeMillis() - prost0team.com.hacktask.DateUtils.ONE_MONTH_IN_MILLS * 3 - 9600000)
        DbService.saveHistory(listOf("1", "4", "2", "3"), time = System.currentTimeMillis() - prost0team.com.hacktask.DateUtils.ONE_MONTH_IN_MILLS * 2 - 5400000)
        Log.d(TAG, "${DbService.getMostPriorityEmailsByEmail(arrayListOf("1"))}")
        Log.d(TAG, "${DbService.getMostPriorityEmailsByEmail(arrayListOf("4"))}")
        Log.d(TAG, "${DbService.getMostPriorityEmailsByEmail(arrayListOf("5"))}")

    }

    companion object {
        val TAG = "#MY " + TaskListActivity::class.java.simpleName
    }
}