package prost0team.com.hacktask.tasklist

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import prost0team.com.hacktask.DateUtils
import prost0team.com.hacktask.base.BasePresenter
import prost0team.com.hacktask.db.DbService
import prost0team.com.hacktask.db.DbTask
import prost0team.com.hacktask.model.TaskViewModel

/**
 * Created by root on 20.04.18.
 */
class TaskListPresenter(val view: ITasksListView): BasePresenter(view) {
    fun getTasks() {
        DbService.getAllTasks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ tasks ->
                    view.showTasks(toViewModels(tasks))
                }, {error -> Log.e(TAG, error.message, error)})
    }

    private fun toViewModels(dbTasks: List<DbTask>): List<TaskViewModel> {
        val tasks = ArrayList<TaskViewModel>(dbTasks.size)
        dbTasks.forEach { dbTask ->
            val taskTime = dbTask.startTime
            val minTime = System.currentTimeMillis() - DateUtils.ONE_DAY_IN_MILLS
            val maxTime = System.currentTimeMillis() + DateUtils.ONE_MONTH_IN_MILLS

            var increment = 0L
            if (taskTime < maxTime) {
                while (taskTime - increment > minTime) {
                    Log.d(TAG, "added less ${dbTask.text} minTime = $minTime, now = ${taskTime-increment}")
                    tasks.add(TaskViewModel.toViewModelForm(dbTask, dbTask.startTime - increment, dbTask.endTime - increment))
                    if (dbTask.period == 0) {
                        break
                    }
                    increment += dbTask.period * DateUtils.ONE_DAY_IN_MILLS
                }

                increment = dbTask.period * DateUtils.ONE_DAY_IN_MILLS

                if (dbTask.period > 0) {
                    while (taskTime + increment < maxTime) {
                        if (taskTime + increment > minTime) {
                            Log.d(TAG, "added more ${dbTask.text}")
                            tasks.add(TaskViewModel.toViewModelForm(dbTask, dbTask.startTime + increment, dbTask.endTime + increment))
                        }
                        if (dbTask.period == 0) {
                            break
                        }
                        increment += dbTask.period * DateUtils.ONE_DAY_IN_MILLS
                    }
                }
            }
        }
        return tasks.sortedBy { it.startTime }
    }

    companion object {
        val TAG = "#MY " + TaskListPresenter::class.java.simpleName
    }
}