package prost0team.com.hacktask.db;

import android.util.Log;

import org.jetbrains.annotations.Nullable;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by root on 20.04.18.
 */

public class DbTask extends RealmObject {
    @PrimaryKey
    private String uuid;
    private String text;
    private Long startTime;
    private Long endTime;
    private Long deadline;
    private Boolean isSounded;
    private Byte type;
    private Byte status;
    private Integer period;
    private DbTag tag;

    public DbTask() {}

    public DbTask(String text,
                  Long startTime,
                  Long endTime,
                  @Nullable Long deadline,
                  Boolean isSounded,
                  Byte type,
                  Byte status,
                  Integer period,
                  DbTag tag
    ) {
        this.uuid = UUID.randomUUID().toString();
        Log.d("#MY ", "generated uuid: " + this.uuid);
        this.text = text;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deadline = deadline;
        this.isSounded = isSounded;
        this.type = type;
        this.status = status;
        this.period = period;
        this.tag = tag;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Boolean getSounded() {
        return isSounded;
    }

    public void setSounded(Boolean sounded) {
        isSounded = sounded;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getDeadline() {
        return deadline;
    }

    public void setDeadline(Long deadline) {
        this.deadline = deadline;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public DbTag getTag() {
        return tag;
    }

    public void setTag(DbTag tag) {
        this.tag = tag;
    }
}
