package prost0team.com.hacktask.db

import android.util.Log
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmConfiguration
import prost0team.com.hacktask.pin.PinHelper
import prost0team.com.hacktask.utils.PriorityUtils
import java.nio.charset.Charset
import java.security.MessageDigest


/**
 * Created by root on 20.04.18.
 */
object DbService {
    fun createTask(dbTask: DbTask): Single<DbTask> {
        return Single.fromCallable {
            val realm = getEncryptedInstance()
            realm.beginTransaction()
            realm.insertOrUpdate(dbTask)
            realm.commitTransaction()
            dbTask
        }
    }

    fun getAllTasks(): Single<List<DbTask>> {
        return Single.just(
                getEncryptedInstance().where(DbTask::class.java).findAll()
        )
    }

    fun getAllTags(): Single<List<DbTag>> {
        return Single.just(
                getEncryptedInstance().where(DbTag::class.java).findAll()
        )
    }

    fun saveAllTasks(dbTasks: List<DbTask>): Single<List<DbTask>> {
        return Single.fromCallable {
            val realm = getEncryptedInstance()
            realm.beginTransaction()
            realm.copyToRealm(dbTasks)
            realm.commitTransaction()
            dbTasks
        }
    }

    fun updateTask(dbTask: DbTask): Single<DbTask> {
        return Single.fromCallable {
            val realm = getEncryptedInstance()
            realm.beginTransaction()
            realm.copyToRealm(dbTask)
            realm.commitTransaction()
            dbTask
        }
    }

    fun deleteTask(uuid: String): Single<Int> {
        return Single.fromCallable {
            val realm = getEncryptedInstance()
            realm.beginTransaction()
            realm.where(DbTask::class.java).equalTo("uuid", uuid).findFirst()?.deleteFromRealm()
            realm.commitTransaction()
            1
        }
    }

    fun dropDb() {
        getEncryptedInstance().beginTransaction()
        getEncryptedInstance().deleteAll()
        getEncryptedInstance().commitTransaction()
    }

    fun getMostPriorityEmailsByEmail(emails: List<String>): List<String> {
        val histories = getEncryptedInstance().where(DbHistory::class.java).findAll().filter {
            Log.e("log","size =" + it.emails.size)
            it.emails.containsAll(emails)
        }
        if (histories.isEmpty()) {
            return listOf()
        }
        var maxPriority = 0
        var maxPosition = 0
        val hashMap = HashMap<String, ArrayList<DbHistory>>()
        histories.forEach {
            val key = DbService.joinToString(it.emails)
            if (hashMap.containsKey(key)) {
                hashMap[key]!!.add(it)
            } else {
                val list = ArrayList<DbHistory>()
                list.add(it)
                hashMap[key] = list
            }
        }
        val values = hashMap.values.toList()
        values.forEachIndexed { index, list ->
            val priority = PriorityUtils.priority(list.map { it.time }.toTypedArray().sortedArray())
            Log.d("#MY ", "prority for ${list.map { it.time }.toTypedArray().sortedArray().joinToString()} = $priority")
            if (priority > maxPriority) {
                maxPriority = priority
                maxPosition = index
            }
        }
        var hintEmails = values[maxPosition][0].emails
        Log.e("log", "hintEmails size " + hintEmails.size)
        Log.e("log", "emails size " + emails.size)
        var distinctHintEmails: MutableList<String> = ArrayList()
        hintEmails.forEach {
            if (!emails.contains(it)) {
                distinctHintEmails.add(it)
            }
        }
        Log.e("log", "distinct size " + distinctHintEmails.size)

        return distinctHintEmails
    }

    fun saveHistory(emails: List<String>, time: Long = System.currentTimeMillis()) {
        val realm = getEncryptedInstance()
        realm.beginTransaction()
        realm.copyToRealm(DbHistory(emails, time))
        realm.commitTransaction()
//        realm.where(DbHistory::class.java).findAll().filter { it.emails.containsAll(emails) && it.emails.size == emails.size }
    }

    fun getEncryptionKey(): ByteArray {
        val sault = PinHelper.getSault()!! + PinHelper.pin
//        Log.d("#MY ", "sault got $sault")

        val md = MessageDigest.getInstance("SHA-512")
        val digest: ByteArray = md.digest(sault.toByteArray(Charset.defaultCharset()))
//        Log.d("#MY ", "digest ${String(digest)} ${digest.size}")
//        val sb = StringBuilder()
//        for (i in digest.indices) {
//            sb.append(Integer.toString((digest[i] and 0xff.toByte()) + 0x100, 16).substring(1))
//        }
//
//        Log.d("#MY ", "hash generated $sb | ${sb.length}")
        return digest
    }

    var config: RealmConfiguration? = null
        get() {
            if (field == null) {
                field = RealmConfiguration.Builder()
                        .name("encrypted")
                        .encryptionKey(getEncryptionKey())
                        .schemaVersion(1)
                        .deleteRealmIfMigrationNeeded()
                        .build()
            }
            return field!!
        }
//    get() = {
//    }

    fun getEncryptedInstance(): Realm {
        return Realm.getInstance(config)
    }

    fun joinToString(list: List<String>): String {
        return list.sorted().joinToString(separator = ", ")
    }

    fun splitToArray(emails: String): List<String> {
        return emails.split(", ")
    }
}