package prost0team.com.hacktask.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by root on 20.04.18.
 */

public class DbTag extends RealmObject {
    @PrimaryKey
    private String name;
    private String color;

    public DbTag() {}

    public DbTag(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
