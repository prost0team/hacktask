package prost0team.com.hacktask.db;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by root on 21.04.18.
 */

public class DbHistory extends RealmObject {
    private String emails;
    private Long time;

    public DbHistory() {}

    public DbHistory(List<String> emails, Long time) {
        this.emails = DbService.INSTANCE.joinToString(emails);
        this.time = time;
    }

    public List<String> getEmails() {
        return DbService.INSTANCE.splitToArray(emails);
    }

    public void setEmails(List<String> emails) {
        this.emails = DbService.INSTANCE.joinToString(emails);
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
