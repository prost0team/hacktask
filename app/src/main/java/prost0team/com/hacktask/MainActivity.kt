package prost0team.com.hacktask.pin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import prost0team.com.hacktask.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}