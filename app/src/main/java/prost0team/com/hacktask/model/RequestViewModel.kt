package prost0team.com.hacktask.model

import java.io.Serializable

class RequestViewModel(
        val type: Byte,
        val text: String,
        val emailFrom: String,
        val startTime: Long?,
        val endTime: Long?,
        val deadline: Long?) : Serializable {


    companion object {
        val TAG = "#MY " + TaskViewModel::class.java.simpleName


        fun toFcmForm(text: String, type: Byte, deadline: Long, startTime: Long, endTime: Long, emailFrom: String, emailTo: String, hasDeadline: Boolean): HashMap<String, Any> {
            val request: HashMap<String, Any> = HashMap()
            request.put("type", type.toInt())
            request.put("text", text)
            request.put("emailFrom", emailFrom)
            request.put("emailTo", emailTo)
            if (type == 0.toByte()) {
                request.put("startTime", startTime)
                request.put("endTime", endTime)
            } else {
                if (hasDeadline)
                    request.put("deadLine", deadline)
            }
            return request
        }

        fun toViewModelForm(fcmObject: Map<String, Any>): RequestViewModel {
            var model: RequestViewModel = RequestViewModel((fcmObject.get("type") as Long).toByte(),
                    fcmObject.get("text") as String,
                    fcmObject.get("emailFrom") as String,
                    fcmObject.get("startTime") as Long?,
                    fcmObject.get("endTime") as Long?,
                    fcmObject.get("deadLine") as Long?
            )
            return model
        }
    }
}
