package prost0team.com.hacktask.model

import prost0team.com.hacktask.db.DbTag

/**
 * Created by root on 20.04.18.
 */
data class TagViewModel(
        val name: String,
        val color: String
) {
    companion object {
        val TAG = "#MY " + TagViewModel::class.java.simpleName

        fun toViewModelForm(dbTag: DbTag): TagViewModel {
            return TagViewModel(
                    dbTag.name,
                    dbTag.color
            )
        }

    }
}