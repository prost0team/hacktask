package prost0team.com.hacktask.model

import com.google.firebase.auth.FirebaseUser
import prost0team.com.hacktask.db.DbTag
import prost0team.com.hacktask.db.DbTask

/**
 * Created by root on 20.04.18.
 */
data class TaskViewModel(
        val uuid: String,
        val text: String,
        val startTime: Long,
        val endTime: Long,
        val deadline: Long?,
        val isSounded: Boolean,
        val type: Byte,
        val status: Byte,
        val period: Int,
        val tag: TagViewModel
) {

    companion object {
        val TAG = "#MY " + TaskViewModel::class.java.simpleName

        enum class DbTypes(val type: Byte) {
            EVENT(0),
            TASK(1)
        }

        enum class DbStatuses(val status: Byte) {
            NOT_COMPLETED(0),
            COMPLETED(1)
        }

        fun toDbForm(task: TaskViewModel): DbTask {
            return DbTask(
                    task.text,
                    task.startTime,
                    task.endTime,
                    task.deadline,
                    task.isSounded,
                    task.type,
                    task.status,
                    task.period,
                    DbTag(task.tag.name, task.tag.color)
            )
        }

        fun toFcmForm(dbTask: DbTask, firebaseUser: FirebaseUser): HashMap<String, Any> {
            val task: HashMap<String, Any> = HashMap()
            task.put("startTime", dbTask.startTime)
            task.put("endTime", dbTask.endTime)
            task.put("deadline", dbTask.deadline)
            task.put("period", dbTask.period)
            task.put("sounded", dbTask.sounded)
            var tag = dbTask.tag
            val tagMap: HashMap<String, Any> = HashMap()
            tagMap.put("name", tag.name)
            tagMap.put("color", tag.color)
            task.put("tag", tagMap)
            task.put("status", dbTask.status.toInt())
            task.put("text", dbTask.text)
            task.put("type", dbTask.type.toInt())
            task.put("user_id", firebaseUser.uid)
            return task
        }

        fun toViewModelForm(dbTask: DbTask): TaskViewModel {
            return TaskViewModel(
                    dbTask.uuid,
                    dbTask.text,
                    dbTask.startTime,
                    dbTask.endTime,
                    dbTask.deadline,
                    dbTask.sounded,
                    dbTask.type,
                    dbTask.status,
                    dbTask.period,
                    TagViewModel(dbTask.tag.name, dbTask.tag.color)
            )
        }

        fun toViewModelForm(dbTask: DbTask, startTime: Long, endTime: Long): TaskViewModel {
            return TaskViewModel(
                    dbTask.uuid,
                    dbTask.text,
                    startTime,
                    endTime,
                    dbTask.deadline,
                    dbTask.sounded,
                    dbTask.type,
                    dbTask.status,
                    dbTask.period,
                    TagViewModel(dbTask.tag.name, dbTask.tag.color)
            )
        }
    }
}