package prost0team.com.hacktask.base

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import prost0team.com.hacktask.createtask.CreateTaskActivity
import prost0team.com.hacktask.model.RequestViewModel

abstract class BaseActivity : AppCompatActivity(), BaseView {

    protected open val presenter: BasePresenter = BasePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentViewLayoutRes)
    }


    override fun onStart() {
        super.onStart()
        presenter.onCreate()
    }

    override fun onStop() {
        super.onStop()
        presenter.onDestroy()
    }

    protected abstract var contentViewLayoutRes: Int

    override fun createRequest(model: RequestViewModel) {
        val intent = Intent(this, CreateTaskActivity::class.java)
        intent.putExtra("model", model)
        startActivity(intent)
    }
}
