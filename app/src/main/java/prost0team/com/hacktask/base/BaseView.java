package prost0team.com.hacktask.base;

import org.jetbrains.annotations.NotNull;

import prost0team.com.hacktask.model.RequestViewModel;

public interface BaseView {
    void createRequest(@NotNull RequestViewModel toViewModelForm);
}
