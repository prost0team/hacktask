package prost0team.com.hacktask.base

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.QuerySnapshot
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import prost0team.com.hacktask.model.RequestViewModel
import prost0team.com.hacktask.settings.ISettingsView
import prost0team.com.hacktask.settings.SettingsPresenter


open class BasePresenter(private val view: BaseView) {

    private val compositeDisposable = CompositeDisposable()

    private var firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    protected var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    protected var firebaseUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser
    private var listener: ListenerRegistration? = null
    private var authListener: FirebaseAuth.AuthStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
        firebaseUser = firebaseAuth.currentUser
        authChangeListener()
    }

    open fun authChangeListener() {
        if (firebaseUser != null)
            createRequestListener()
    }

    open fun onCreate() {
        firebaseAuth.addAuthStateListener(authListener)
        authChangeListener()
    }

    open fun createRequestListener() {
        if (listener != null)
            listener!!.remove()
        if (firebaseUser != null) {
            listener = firestore.collection("requests")
                    .whereEqualTo("emailTo", firebaseUser!!.email)
                    .addSnapshotListener(EventListener<QuerySnapshot> { snapshot, e ->
                        if (e != null) {
                            Log.w(SettingsPresenter.TAG, "Listen failed.", e)
                            return@EventListener
                        }
                        if (snapshot != null && !snapshot.isEmpty()) {
                            snapshot.documents.forEach {
                                if (it.data != null) {
                                    firestore.collection("requests")
                                            .document(it.id)
                                            .delete()
                                            .addOnCompleteListener {
                                            }
                                    view.createRequest(RequestViewModel.toViewModelForm(it.data!!))
                                }
                            }
                            Log.d(SettingsPresenter.TAG, "Current data: " + snapshot.documents.size)
                        } else {
                            Log.d(SettingsPresenter.TAG, "Current data: null")
                        }
                    })
        }
    }

    open fun onDestroy() {
        compositeDisposable.clear()
        firebaseAuth.removeAuthStateListener(authListener)
        if (listener != null)
            listener!!.remove()
    }

    protected fun Disposable.connect() {
        compositeDisposable.add(this)
    }


}