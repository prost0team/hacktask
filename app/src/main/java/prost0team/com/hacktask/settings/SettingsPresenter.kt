package prost0team.com.hacktask.settings

import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import prost0team.com.hacktask.base.BasePresenter
import java.util.*
import java.nio.file.Files.exists


/**
 * Created by root on 05.04.18.
 */
class SettingsPresenter(private val view: ISettingsView) : BasePresenter(view) {
    fun getCurrentUser(): FirebaseUser? {
        return firebaseUser
    }

    fun checkState() {
        firebaseUser = FirebaseAuth.getInstance().currentUser
        view.updateLoggedStatus(firebaseUser != null)
    }

    fun quit() {
        FirebaseAuth.getInstance().signOut()
    }

    override fun onCreate() {
        super.onCreate()
        firebaseAuth.addAuthStateListener(authListener)
        if (firebaseUser != null) {

        }
    }

//    private fun addChangeListener(email: String) {
//        firestore.collection("requests")
//                .whereEqualTo("emailTo", email)
//                .addSnapshotListener(EventListener<QuerySnapshot> { snapshot, e ->
//                    if (e != null) {
//                        Log.w(TAG, "Listen failed.", e)
//                        return@EventListener
//                    }
//                    if (snapshot != null && !snapshot.isEmpty()) {
//                        Log.d(TAG, "Current data: " + snapshot.documents.size)
//                    } else {
//                        Log.d(TAG, "Current data: null")
//                    }
//                })
//    }

    override fun onDestroy() {
        super.onDestroy()
        firebaseAuth.removeAuthStateListener(authListener)
    }

    private var firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var authListener: FirebaseAuth.AuthStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
        firebaseUser = firebaseAuth.currentUser
        view.updateLoggedStatus(firebaseUser != null)
    }

    fun createRequest(type: Int, time: Long, taskName: String, email: String) {
        if (firebaseUser != null) {
            val user: HashMap<String, Any> = HashMap()
            user.put("emailTo", email)
            user.put("emailFrom", firebaseUser!!.email!!)
            user.put("taskName", taskName)
            user.put("time", time)
            user.put("type", type)
            firestore.collection("requests")
                    .add(user)
                    .addOnCompleteListener {
                        Log.e("ok", "complete")
                    }
                    .addOnFailureListener(OnFailureListener {
                        Log.e("ok", "fail", it)

                    })
        }
    }

    private fun getIncomingRequests(email: String) {
        firestore.collection("requests")
                .whereEqualTo("emailTo", email)
                .get()
                .addOnCompleteListener {
                    for (document in it.result) {
                        Log.d(TAG, document.id + " => " + document.data)
                    }
                }
                .addOnFailureListener(OnFailureListener {
                    Log.e("ok", "fail", it)

                })
    }


    companion object {
        val TAG = "#MY " + SettingsPresenter::class.java.simpleName
    }
}