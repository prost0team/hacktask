package prost0team.com.hacktask.settings;

import prost0team.com.hacktask.base.BaseView;

public interface ISettingsView extends BaseView{
    public void updateLoggedStatus(boolean isLoggedIn);
}
