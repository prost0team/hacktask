package prost0team.com.hacktask.settings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.util.Log
import android.view.View
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import kotlinx.android.synthetic.main.activity_settings.*
import prost0team.com.hacktask.R
import prost0team.com.hacktask.base.BaseActivity
import java.util.*


class SettingsActivity : BaseActivity(), ISettingsView {

    override var contentViewLayoutRes: Int = R.layout.activity_settings

    override val presenter: SettingsPresenter = SettingsPresenter(this)

    private val FIREBASE_SIGN_IN = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        toolbar.setTitle("Настройки")
        auth_btn.setOnClickListener {
            signInWithFirebase()
        }
        quit_btn.setOnClickListener {
            presenter.quit()
        }
    }

    override fun updateLoggedStatus(loggedIn: Boolean) {
        if (loggedIn) {
            auth_state.text = presenter.getCurrentUser()!!.email
            quit_btn.visibility = View.VISIBLE
            auth_btn.visibility = View.GONE
        } else {
            auth_state.setText(R.string.not_autorized)
            quit_btn.visibility = View.GONE
            auth_btn.visibility = View.VISIBLE
        }
    }


    fun signInWithFirebase() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Arrays.asList(
                                        AuthUI.IdpConfig.GoogleBuilder().build()))
                        .build(),
                FIREBASE_SIGN_IN)
    }

    override fun onResume() {
        super.onResume()
        presenter.checkState()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FIREBASE_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            // Successfully signed in
            if (resultCode == Activity.RESULT_OK) {
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    showError(R.string.sign_in_cancelled)
                    return
                }

                if (response.error!!.errorCode == ErrorCodes.NO_NETWORK) {
                    showError(R.string.no_internet_connection)
                    return
                }

                showError(R.string.unknown_error)
                Log.e("error", "error", response.error)
            }
        }
    }

    private fun showError(@StringRes errorMessageRes: Int) {
        Toast.makeText(this, errorMessageRes, Toast.LENGTH_LONG).show()
    }
}
