package prost0team.com.hacktask.utils

import android.util.Log
import java.lang.Math.abs
import java.lang.Math.sqrt

/**
 * Created by root on 21.04.18.
 */
object PriorityUtils {
    fun priority(t:Array<Long>):Int{
        val time = System.currentTimeMillis()
        val n = t.size
        val x = LongArray(n,{0})
        var tav:Long = 0 //Среднее время
        for(i:Int in t.indices){
            tav +=abs (t[i]%86400000 - time%86400000)
            x[i] = abs(t[i]%86400000 - time%86400000)
        }
        tav /= n
        var s= 0.0
        for(i:Int in x.indices){
            s += (x[i] - tav) * (x[i] - tav)
        }
        s /= (n * (n - 1))
        s = sqrt(s)
        return if (s<1){
            n
        }
        else{
            Log.i("debug",(n*tav/abs(t[n-1]%86400000 - time%86400000)).toString())
            (n*tav/abs(t[n-1]%86400000 - time%86400000)).toInt()
        }
    }
}